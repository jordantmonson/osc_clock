from datetime import datetime, timezone, timedelta
import pypco
import time

pco_dict = {}
pco = pypco.PCO("8134c29b91f872ddbc2b2437b84c3aa13877dfb5846be38f9d6aa89e28c2fa80", "5eee9b4fa6ad923bfcb412be9ed0fa9c4a42547c09fbab2b09329f1cff2838d7")
service_id = ''


def convert_time(ct):
    ct = datetime.strptime(ct, '%Y-%m-%d %H:%M:%S')
    lt = utc_to_local(ct)
    return str(lt)


def utc_to_local(dt):
    if time.localtime().tm_isdst:
        return dt - timedelta(seconds=time.altzone)
    else:
        return dt - timedelta(seconds=time.timezone)


for service in pco.iterate('/services/v2/service_types/15087/plans', filter='future'):
    service_id = service['data']['id']
    break

print(service_id)

for plan_times in pco.get('/services/v2/service_types/15087/plans/' + service_id, include='plan_times')['included']:
    if plan_times['attributes']['time_type'] == "rehearsal":
        name = plan_times['attributes']['name']
        #utc = plan_times['attributes']['starts_at'].split('T')
        utc_list = plan_times['attributes']['starts_at'].strip('Z').split('T')
        utc = ' '.join(utc_list)
        local_time = convert_time(utc)
        pco_dict[name] = str(local_time)

for key, value in pco_dict.items():
    print(key, value)

