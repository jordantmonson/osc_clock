Repo for my OSC powered HTTP Clock


#Build instructions for Docker
cd ~/docker_osc_clock

sudo docker image build -t osc_clock .

#Run instructions for Docker
sudo docker run -dit --restart unless-stopped -p 5005:5005/udp -p 8888:8888 osc_clock

#Docker helpful commands

    #Show all running docker containers
    sudo docker ps

    #Stop Docker container
    sudo docker stop ####Container-ID####
