FROM python:3

COPY . /app
COPY templates /app
COPY static /app
WORKDIR /app

ENV TZ America/Chicago
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN pip install python-osc
RUN pip install flask
RUN pip install pypco
RUN pip install Flask-SocketIO==4.3.2
RUN pip install waitress

CMD [ "python", "/app/osc_clock.py" ]
