#from twisted.internet import reactor
#from twisted.web.server import Site
#rom twisted.web.resource import Resource
import time
from oscpy.server import OSCThreadServer

osc = OSCThreadServer()
sock = osc.listen(address='0.0.0.0', port=50005, default=True)

osc_time = ''
name = ''
text_color = '2196f3'
font_size = '240'
time_size = '350'

@osc.address(b'/clock/timer')
def callback_name(values):
    global osc_time
    osc_timer = get_sec(values)
    seconds = get_sec(time.strftime("%H:%M:%S", time.localtime()))

    osc_time = sec2time(osc_timer + seconds)
    time_math()

@osc.address(b'/clock/name_size')
def callback_name(values):
    global font_size
    font_size = str(values)

@osc.address(b'/clock/time_size')
def callback_name(values):
    global time_size
    time_size = str(values)

@osc.address(b'/clock/color')
def callback_name(values):
    global text_color
    if len(values) == 6:
        text_color = str(values)

@osc.address(b'/clock/name')
def callback_name(values):
    global name
    global text_color
    text_color = '2196f3'
    if values:
        name = str(values)
    else:
        name = ''

@osc.address(b'/clock/time')
def callback_time(values):
    global osc_time
    osc_time = values
    time_math()

def get_sec(time_str):  # Get Seconds from time
    h, m, s = time_str.split(':')
    return int(h) * 3600 + int(m) * 60 + int(s)


def sec2time(seconds):
    m, s = divmod(seconds, 60)
    h, m = divmod(m, 60)

    return str("%02d" % (h,)) + ':' + str("%02d" % (m,)) + ':' + str("%02d" % (s,))


def time_math():
    negative = False
    new_time = [0,0,0]
    new_sec = 0
    if not osc_time == '':
        current_time = time.strftime("%H:%M:%S", time.localtime())

        osc_sec = get_sec(osc_time)
        current_sec = get_sec(current_time)

        new_sec = osc_sec - current_sec
        if new_sec < 0:
            negative = True
            new_sec = current_sec - osc_sec

        new_time = '<div style="font-size:' + time_size + 'px;">' + str(sec2time(new_sec))
        if negative:
            new_time = '<div style="color: red;' + 'font-size:' + time_size + 'px;">-' + str(sec2time(new_sec))
        return new_time
    else:
        return ''


class ClockPage(Resource):
    global name
    isLeaf = True
    def render_GET(self, request):
        return '<head><meta http-equiv="refresh" content="1"></head><html style="font-family: arial;font-size:' + font_size + 'px;' \
               'background: black;color: #' + text_color + ';text-align: center; padding-top: 0%;">' \
               '<body><div>' + name + '<br>' + time_math() + '</div></body></html>'

#resource = ClockPage()
#factory = Site(resource)
#reactor.listenTCP(8880, factory)
#reactor.run()
