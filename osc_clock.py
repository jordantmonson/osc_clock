from datetime import datetime, timedelta
from pythonosc import osc_server
import argparse

from flask_socketio import SocketIO
from flask import Flask, render_template
from threading import Thread, Event
import pypco
import time
from waitress import serve
import logging

# Setup loggin to a file
logging.basicConfig(format='%(asctime)s %(message)s', filename='osc_clock.log', level=logging.WARNING)

# PCO developer information.
pco = pypco.PCO("8134c29b91f872ddbc2b2437b84c3aa13877dfb5846be38f9d6aa89e28c2fa80",
                "5eee9b4fa6ad923bfcb412be9ed0fa9c4a42547c09fbab2b09329f1cff2838d7")

'''#####################################
###  Defaults for the web Rendering  ###
#####################################'''

text_color = '2196f3'
font_size = '240'
time_size = '350'

# Setup some Variables for later use
osc_time = ''
name = ''
pco_dict = {}

# Setup Socketio and the threads for everything
app = Flask(__name__)
app.config['SECRET_KEY'] = 'secret!'
socketio = SocketIO(app)

thread = Thread()  # random number Generator Thread
osc_t = Thread()  # osc server Thread
pco_t = Thread()  # Thread to update pco values
thread_stop_event = Event()

'''
###########################################
Callbacks for the OSC functionality
###########################################
'''


def callback_timer(ua, values):
    global osc_time
    global text_color
    text_color = 'ffff00'
    osc_time = str(datetime.today() + timedelta(minutes=int(values))).split('.')[0]
    time_html()


def callback_name_size(ua, values):
    global font_size
    font_size = str(values)


def callback_time_size(ua, values):
    global time_size
    time_size = str(values)


def callback_color(ua, values):
    global text_color
    if len(values) == 6:
        text_color = str(values)


def callback_name(ua, values):
    global name
    global text_color
    text_color = '2196f3'
    if values:
        name = str(values)
    else:
        name = ''

def callback_name_test(ua):
    print(ua)

def callback_time(ua, values):
    global osc_time
    if values:
        osc_time = make_24h(values)
        time_html()
    else:
        osc_time = ''


def callback_pco(ua, values):
    global name
    global text_color
    global osc_time
    text_color = '2196f3'
    if values in pco_dict:
        name = values
        osc_time = pco_dict[values]
    else:
        name = ''


'''
###########################################
Helper Functions for the rest of the Script
###########################################
'''


def convert_time(ct):
    ct = datetime.strptime(ct, '%Y-%m-%d %H:%M:%S')
    lt = utc_to_local(ct)
    return str(lt)


def utc_to_local(dt):
    if time.localtime().tm_isdst:
        return dt - timedelta(seconds=time.altzone)
    else:
        return dt - timedelta(seconds=time.timezone)


def pull_pco():
    while not thread_stop_event.isSet():
        global pco_dict
        service_id = ''
        for service in pco.iterate('/services/v2/service_types/15087/plans', filter='future'):
            service_id = service['data']['id']
            break

        pco_plan = pco.get('/services/v2/service_types/15087/plans/' + service_id, include='plan_times')

        for plan_times in pco_plan['included']:

            if plan_times['attributes']['time_type'] == "rehearsal":
                time_name = plan_times['attributes']['name']
                utc_list = plan_times['attributes']['starts_at'].strip('Z').split('T')
                utc = ' '.join(utc_list)
                local_time = convert_time(utc)
                pco_dict[time_name] = str(local_time)

            if plan_times['attributes']['time_type'] == "service":
                time_name = plan_times['attributes']['name']
                utc_list = plan_times['attributes']['starts_at'].strip('Z').split('T')
                utc = ' '.join(utc_list)
                local_time = convert_time(utc)
                pco_dict[time_name] = str(local_time)

        pco_items = pco.get('/services/v2/service_types/15087/plans/' + service_id + '/items')['data']

        start_time_delta = 0
        start_time = 0

        # Get actual rehearsal start time.
        for each in pco_items:
            if each['attributes']['title'] == 'PRE-SERVICE':
                if 'Service Recording' in pco_dict:
                    start_time = make_time(pco_dict['Service Recording']) - timedelta(seconds = start_time_delta)
                elif 'Thursday recording' in pco_dict:
                    start_time = make_time(pco_dict['Thursday recording']) - timedelta(seconds = start_time_delta)
                elif 'Thursday Recording' in pco_dict:
                    start_time = make_time(pco_dict['Thursday Recording']) - timedelta(seconds = start_time_delta)
                logging.warning(start_time)
                break
            start_time_delta += each['attributes']['length']
        for each in pco_items:
            if each['attributes']['title'] == 'PRE-SERVICE':
                break
            pco_dict[each['attributes']['title']] = str(start_time)
            start_time = (start_time + timedelta(seconds = each['attributes']['length']))
        time.sleep(10)

def make_time(ct):
    ct = datetime.strptime(ct, '%Y-%m-%d %H:%M:%S')
    return ct


def make_24h(lt):
    date_lt = str(datetime.today()).split()[0] + ' ' + lt
    if 'PM' in lt:
        return str(datetime.strptime(date_lt, '%Y-%m-%d %I:%M:%S %p'))
    elif 'AM' in lt:
        return str(datetime.strptime(date_lt, '%Y-%m-%d %I:%M:%S %p'))
    else:
        return date_lt


def time_math(a, b):
    a = make_time(a)

    if a > b:
        delta = a - b
        negative = False
    else:
        delta = b - a
        negative = True

    if negative and datetime.strptime(str(delta).split('.')[0], '%H:%M:%S') > datetime.strptime('02:00:00', '%H:%M:%S'):
        delta = (a + timedelta(days=1)) - b
        negative = False
    return str(delta).split('.')[0], negative


def time_html():
    try:
        logging.warning(osc_time)
        if not osc_time == '':
            local_time = datetime.today()
            delta = time_math(osc_time, local_time)

            if delta[1]:
                new_time = '<div style=\"color: red;' + 'font-size:' + time_size + 'px;\">-' + delta[0]
            else:
                new_time = '<div style=\"font-size:' + time_size + 'px;\">' + delta[0]
            return new_time
        else:
            return ''
    except:
        return '<div style=\"color: red;' + 'font-size:' + str(int(time_size) / 2) + 'px;\">' + 'INVALID OSC MESSAGE'


def render_get():
    return '<div style=\"color: #' + text_color + '">' + name + '<br>' + time_html() + '</div>'


def osc_thread():  # OSC Thread for handling osc requests
    from pythonosc import dispatcher
    parser = argparse.ArgumentParser()
    parser.add_argument("--ip", default="0.0.0.0", help="The ip to listen on")
    parser.add_argument("--port", type=int, default=5005, help="The port to listen on")
    args = parser.parse_args()

    dispatcher = dispatcher.Dispatcher()
    dispatcher.map("/clock/name", callback_name)
    dispatcher.map("/clock/timer", callback_timer)
    dispatcher.map("/clock/name_size", callback_name_size)
    dispatcher.map("/clock/time_size", callback_time_size)
    dispatcher.map("/clock/color", callback_color)
    dispatcher.map("/clock/time", callback_time)
    dispatcher.map("/clock/pco", callback_pco)

    server = osc_server.ThreadingOSCUDPServer(
        (args.ip, args.port), dispatcher)
    logging.warning("Serving on {}".format(server.server_address))
    server.serve_forever()


def clock_update():  # Update the clock on the socket
    while not thread_stop_event.isSet():
        number = render_get()
        socketio.emit('new_clock', {'number': number}, namespace='/clock')
        socketio.sleep(0.5)


@app.route('/')
def index():
    return render_template('index.html')


@socketio.on('connect', namespace='/clock')
def test_connect():
    # need visibility of the global thread object
    global thread
    global osc_t
    global pco_t
    logging.warning('Client connected')

    if not thread.is_alive():  # Start the clock if it is not running already
        logging.warning('Starting Thread')
        thread = socketio.start_background_task(clock_update)


@socketio.on('disconnect', namespace='/clock')
def test_disconnect():
    logging.warning('Client disconnected')


if __name__ == '__main__':
    osc_t = socketio.start_background_task(osc_thread)
    pco_t = socketio.start_background_task(pull_pco)
    serve(app, host='0.0.0.0', port=8888, threads=16)
    #socketio.run(app, host='0.0.0.0', port='8888')
